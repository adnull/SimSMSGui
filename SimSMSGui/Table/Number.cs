﻿
namespace SimSMSGui.Table
{
	public class Phone
	{
		public string Id { get; set; }
		public string Country { get; set; }
		public string Service { get; set; }
		public string Number { get; set; }
		public string Price { get; set; }
		public string Status { get; set; }
		public string Code { get; set; }
	}
}
