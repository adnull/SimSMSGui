﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using SimSMSGui.Models;

namespace SimSMSGui
{
	public class Utils
	{
		public static List<T> LoadCsv<T>(string Data) where T : new()
		{
			string[] csv_text = Data.Split('\n');
			var items = new List<T>();

			foreach (string item in csv_text)
			{
				items.Add((T)Activator.CreateInstance(typeof(T), new object[] { item.Split(',') }));
			}

			return items;
		}
		public static bool ValidApiKey()
		{
			if (Data.ApiKey == null || Data.ApiKey == String.Empty)
				return false;

			Regex rx = new Regex("[a-zA-Z0-9]{30}");

			return rx.IsMatch(Data.ApiKey);
		}
		public static Country GetCountry(string Name)
		{
			Country target_country = new Country();

			foreach (Country country in Data.Countries)
			{
				if (country.Name == Name)
				{
					target_country = country;
				}
			}

			return target_country;
		}
		public static Service GetService(string Service)
		{
			Service target_service = new Service();

			foreach (Service service in Data.Services)
			{
				if (service.Name == Service)
				{
					target_service = service;
				}
			}

			return target_service;
		}
		public static string GetApiKey()
		{
			var app_data = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			var filename = Path.Combine(app_data, "SimSMS\\User.json");

			if (!File.Exists(filename))
				return null;

			string text = System.IO.File.ReadAllText(filename);

			try
			{
				dynamic json_data = JObject.Parse(text);

				if (json_data["apikey"] == null)
					return null;

				return json_data["apikey"];
			}
			catch (Newtonsoft.Json.JsonReaderException) {
				return null;
			}
		}
		public static void DumpApiKey(string ApiKey)
		{
			var app_data = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
			var app_folder = Path.Combine(app_data, "SimSMS");

			if (!Directory.Exists(app_folder))
			{
				System.IO.Directory.CreateDirectory(app_folder);
			}

			var app_file = Path.Combine(app_folder, "User.json");

			if (!File.Exists(app_file))
			{
				System.IO.File.Create(app_file);
			}

			string text = System.IO.File.ReadAllText(app_file);
			JObject json_data = new JObject();

			try
			{
				json_data = JObject.Parse(text);
			}
			catch (Newtonsoft.Json.JsonReaderException)
			{
				// Use new JSON object instead
			}

			json_data["apikey"] = ApiKey;

			File.WriteAllText(app_file, json_data.ToString());
		}
	}
}
