﻿using SimSMSGui.Models;

namespace SimSMSGui
{
	class Data
	{
		public static Country[] Countries = Utils.LoadCsv<Country>(Properties.Resources.Countries).ToArray();
		public static Service[] Services = Utils.LoadCsv<Service>(Properties.Resources.Services).ToArray();
		public static string ApiKey;
	}
}
