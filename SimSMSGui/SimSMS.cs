﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using SimSMSGui.Models;

namespace SimSMSGui
{
	class SimSMS
	{
		private HttpClient client;
		public SimSMS()
		{
			client = new HttpClient();
		}
		public async Task<dynamic> ApiCall(string Method, string Country = "", string Service = "", string Operator = "", string Id = "")
		{
			HttpResponseMessage response;
			FormUrlEncodedContent form_content;

			form_content = new FormUrlEncodedContent(new[]
			{
				new KeyValuePair<string, string>("metod", Method), // Spelling mistake here is intentional
				new KeyValuePair<string, string>("service", Service),
				new KeyValuePair<string, string>("country", Country),
				new KeyValuePair<string, string>("apikey", Data.ApiKey),
				new KeyValuePair<string, string>("operator", Operator),
				new KeyValuePair<string, string>("id", Id)
			});

			string query_string = form_content.ReadAsStringAsync().Result;
			response = await client.GetAsync("http://simsms.org/priemnik.php?" + query_string);

			return JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());
		}
		public async Task<decimal> ServiceCost(string Service, string Country)
		{
			dynamic response = await ApiCall("get_service_price", Country: Country, Service: Service);
			return Convert.ToDecimal(response["price"]);
		}
		public async Task<User> UserInfo()
		{
			dynamic response = await ApiCall("get_userinfo", Service: "opt4");

			User user = new User();

			if (response["error"] != null)
				return user;

			user.Balance = (decimal)response["balance"];
			user.Karma = (decimal)response["karma"];
			user.Valid = true;

			return user;
		}
		public async Task<decimal> Balance()
		{
			dynamic response = await ApiCall("get_balance", Service: "opt4");

			if (response["balance"] == null)
				return 0;

			return (decimal)response["balance"];
		}
		public async Task<dynamic> Number(string Service, string Country)
		{
			return await ApiCall("get_number", Country, Service);
		}
		public async Task<dynamic> Code(string Country, string Service, string Id)
		{
			return await ApiCall("get_sms", Country, Service, Id: Id);
		}
		public async Task<dynamic> Cancel(string Service, string Country, string Id)
		{
			return await ApiCall("denial", Country, Service, Id: Id);
		}
		public async Task<dynamic> Ban(string Service, string Country, string Id)
		{
			return await ApiCall("ban", Country, Service, Id: Id);
		}
	}
}
