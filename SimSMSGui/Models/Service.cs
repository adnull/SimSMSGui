﻿
namespace SimSMSGui.Models
{
	public class Service
	{
		public string Name { get; }
		public string Code { get; }
		public Service() { }
		public Service(string[] CsvArray)
		{
			this.Name = CsvArray[0];
			this.Code = CsvArray[1].Trim();
		}
	}
}
