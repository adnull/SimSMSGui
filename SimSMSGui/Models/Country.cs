﻿
namespace SimSMSGui.Models
{
	public class Country
	{
		public string CyrillicName { get; }
		public string Name { get; }
		public string Code { get; }
		public string Extension { get; }
		public Country() { }
		public Country(string[] CsvArray)
		{
			this.CyrillicName = CsvArray[0];
			this.Name = CsvArray[1];
			this.Code = CsvArray[2];
			this.Extension = CsvArray[3];
		}
	}
}
