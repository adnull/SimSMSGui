﻿
namespace SimSMSGui.Models
{
	class User
	{
		public decimal Balance { get; set; }
		public decimal Karma { get; set; }
		public bool Valid = false;
		public User() { }
	}
}
