﻿using System;
using System.Linq;
using System.Windows;
using System.Threading.Tasks;
using System.Collections.Generic;
using SimSMSGui.Models;

namespace SimSMSGui
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private SimSMS client;
		public MainWindow()
		{
			InitializeComponent();
		}
		private async void WindowLoaded(object sender, RoutedEventArgs e)
		{
			client = new SimSMS();

			foreach (Models.Service service in Data.Services)
			{
				Services.Items.Add(service.Name);
			}

			string apikey = Utils.GetApiKey();

			if (apikey != null)
			{
				UserApiKey.Password = apikey;
				Data.ApiKey = apikey;
			}

			await RefreshUserData();
			await Loop();
		}
		public async Task RefreshUserData()
		{
			if (!Utils.ValidApiKey())
				return;

			User user = await client.UserInfo();

			if (user.Valid)
			{
				Balance.Text = "₽" + user.Balance.ToString("0.00");
				Karma.Text = user.Karma.ToString();
			}
		}
		private async void SaveApiKey(object sender, RoutedEventArgs e)
		{
			try
			{
				Utils.DumpApiKey(UserApiKey.Password);
				Data.ApiKey = UserApiKey.Password;
			}
			catch (System.IO.IOException)
			{
				MessageBox.Show("Unable to save user data as it is open elsewhere.");
			}

			await RefreshUserData();
		}
		private async void GetServices(object sender, RoutedEventArgs e)
		{
			if (!Utils.ValidApiKey())
				return;

			Models.Service service = Utils.GetService(Services.Text);

			if (service.Code == null)
				return;

			ServiceData.ItemsSource = null;

			List<Table.Service> items = new List<Table.Service>();

			int count = 0;

			ServiceProgress.Value = count;
			ServiceProgress.Maximum = Data.Countries.Length;

			foreach (Country country in Data.Countries)
			{
				decimal cost = (decimal)(await client.ServiceCost(service.Code, country.Code));

				if (cost == 0)
					continue;

				items.Add(new Table.Service()
				{ 
					Country = country.Name,
					Name = service.Name,
					Price = "₽" + cost.ToString()
				});

				count += 1;
				ServiceProgress.Value = count;
			}

			ServiceData.ItemsSource = items.OrderBy(o => Convert.ToDecimal(o.Price.Replace("₽", "")));
		}
		private async void RequestNumber(object sender, RoutedEventArgs e)
		{
			if (!Utils.ValidApiKey())
				return;

			Table.Service item = (Table.Service)ServiceData.SelectedItem;

			if (item == null)
				return;

			var confirm = MessageBox.Show("Request number?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);

			if (confirm == MessageBoxResult.No)
				return;

			Models.Country country = Utils.GetCountry(item.Country);
			Models.Service service = Utils.GetService(item.Name);

			if (country == null || service == null)
				return;

			dynamic number = await client.Number(service.Code, country.Code);

			switch (Convert.ToString(number["response"]))
			{
				case "1":
					var number_data = new Table.Phone()
					{
						Id = number["id"],
						Country = item.Country,
						Service = item.Name,
						Number = "+" + country.Extension.ToString() + " " + number["number"].ToString(),
						Price = item.Price,
						Status = "Waiting...",
						Code = String.Empty
					};

					PhoneNumberData.Items.Add(number_data);
					break;
				case "2":
					MessageBox.Show("Numbers are busy, please try again in 30 seconds.");
					break;
				default:
					break;
			}
		}
		private void CopyNumber(object sender, RoutedEventArgs e)
		{
			Clipboard.SetText(((Table.Phone)PhoneNumberData.SelectedItem).Number);
		}
		private void CopyCode(object sender, RoutedEventArgs e)
		{
			Clipboard.SetText(((Table.Phone)PhoneNumberData.SelectedItem).Code);
		}
		private async void CancelNumber(object sender, RoutedEventArgs e)
		{
			Table.Phone item = (Table.Phone)PhoneNumberData.SelectedItem;

			var confirm = MessageBox.Show("Cancel order?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);

			if (confirm == MessageBoxResult.No)
				return;

			Models.Country country = Utils.GetCountry(item.Country);
			Models.Service service = Utils.GetService(item.Service);

			dynamic response = await client.Cancel(service.Code, country.Code, item.Id);

			if (response["response"] == "1")
			{
				item.Status = "Cancelled";
				PhoneNumberData.Items.Refresh();
			}
		}
		private async void BanNumber(object sender, RoutedEventArgs e)
		{
			Table.Phone item = (Table.Phone)PhoneNumberData.SelectedItem;

			var confirm = MessageBox.Show("Ban number?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);

			if (confirm == MessageBoxResult.No)
				return;

			Models.Country country = Utils.GetCountry(item.Country);
			Models.Service service = Utils.GetService(item.Service);

			dynamic response = await client.Cancel(service.Code, country.Code, item.Id);

			if (response["response"] == "1")
			{
				item.Status = "Banned";
				PhoneNumberData.Items.Refresh();
			}
		}
		private async Task Loop()
		{
			Models.Country country;
			Models.Service service;

			for (;;)
			{
				foreach (Table.Phone item in PhoneNumberData.Items)
				{
					if (item.Status != "Waiting...")
						continue;

					country = Utils.GetCountry(item.Country);
					service = Utils.GetService(item.Service);

					if (country == null || service == null)
						return;

					dynamic response = await client.Code(country.Code, service.Code, item.Id);

					switch (Convert.ToString(response["response"]).Trim())
					{
						case "1":
							item.Status = "Finished";
							item.Code = response["sms"];
							break;
						case "3":
							item.Status = "Error";
							break;
						default:
							break;
					}

					PhoneNumberData.Items.Refresh();
					await RefreshUserData();
				}

				await Task.Delay(1000);
			}
		}
	}
}
